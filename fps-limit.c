/*
 * fps-limit - Example program to limit frames per seconds
 *
 * Copyright (C) 2013  Antonio Ospite <ospite@studenti.unina.it>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "fps-limit.h"

#define FPS 60

int main(void)
{
	struct fps_limit_stats stats;
	struct timespec tmp;

	fps_limit_init(&stats, FPS);
	while (1) {
		fps_limit_dbg("Doing some work, minimum: %dns", NSEC_PER_SEC / FPS);

		/* simulate a shorter workload */
		tmp.tv_sec = 0;
		tmp.tv_nsec = (NSEC_PER_SEC / FPS) * 2 / 3;
		nanosleep(&tmp, NULL);

		/* and wait for the delay imposed by the limit */
		fps_limit_sleep(&stats);
	}

	return 0;
}
