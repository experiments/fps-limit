CFLAGS = -std=c99 -pedantic -pedantic-errors -Wall -g3 -O2 -D_ANSI_SOURCE_
CFLAGS += -fno-common \
	  -Wall \
	  -Wdeclaration-after-statement \
	  -Wextra \
	  -Wformat=2 \
	  -Winit-self \
	  -Winline \
	  -Wpacked \
	  -Wp,-D_FORTIFY_SOURCE=2 \
	  -Wpointer-arith \
	  -Wlarger-than-65500 \
	  -Wmissing-declarations \
	  -Wmissing-format-attribute \
	  -Wmissing-noreturn \
	  -Wmissing-prototypes \
	  -Wnested-externs \
	  -Wold-style-definition \
	  -Wredundant-decls \
	  -Wsign-compare \
	  -Wstrict-aliasing=2 \
	  -Wstrict-prototypes \
	  -Wswitch-enum \
	  -Wundef \
	  -Wunreachable-code \
	  -Wunsafe-loop-optimizations \
	  -Wunused-but-set-variable \
	  -Wwrite-strings

CFLAGS += -DDEBUG

# For clock_nanosleep()
CFLAGS += -D_POSIX_C_SOURCE=200112L

# for clock_gettime()
LDLIBS += -lrt

fps-limit: fps-limit.o
fps-limit.o: fps-limit.h

clean:
	rm -rf *~ *.o fps-limit
