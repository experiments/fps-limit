/*
 * fps-limit - Some functions to limit frames per seconds
 *
 * Copyright (C) 2013  Antonio Ospite <ospite@studenti.unina.it>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef FPS_LIMIT_H
#define FPS_LIMIT_H

#include <stdio.h>
#include <string.h>
#include <time.h>

#ifdef DEBUG
#define fps_limit_dbg(...)           \
	do {                         \
		printf(__VA_ARGS__); \
		printf("\n");        \
		fflush(stdout);      \
	} while(0)
#else
#define fps_limit_dbg(...) do {} while(0)
#endif

#define NSEC_PER_SEC 1000000000

#define fps_limit_timespecsub(a, b, result)                      \
	do {                                                     \
		(result)->tv_sec = (a)->tv_sec - (b)->tv_sec;    \
		(result)->tv_nsec = (a)->tv_nsec - (b)->tv_nsec; \
		if ((result)->tv_nsec < 0) {                     \
			--(result)->tv_sec;                      \
			(result)->tv_nsec += 1000000000;         \
		}                                                \
	} while(0)

struct fps_limit_stats {
	struct timespec time_start;
	struct timespec time_end;
	unsigned int delay;
};

static void fps_limit_init(struct fps_limit_stats *stats, unsigned int fps)
{
	memset(stats, 0, sizeof(*stats));

	clock_gettime(CLOCK_MONOTONIC, &stats->time_start);
	fps_limit_dbg("Init time: s: %ld, ns: %ld", stats->time_start.tv_sec, stats->time_start.tv_nsec);

	stats->delay = NSEC_PER_SEC / fps;
}

static void fps_limit_sleep(struct fps_limit_stats *stats)
{
	struct timespec elapsed;

	fps_limit_dbg("Start time: s: %ld, ns: %ld", stats->time_start.tv_sec, stats->time_start.tv_nsec);

	clock_gettime(CLOCK_MONOTONIC, &stats->time_end);
	fps_limit_dbg("End time: s: %ld, ns: %ld", stats->time_end.tv_sec, stats->time_end.tv_nsec);

	fps_limit_timespecsub(&stats->time_end, &stats->time_start, &elapsed);
	fps_limit_dbg("Elapsed s: %ld ns: %ld", elapsed.tv_sec, elapsed.tv_nsec);

	if (elapsed.tv_sec == 0 && elapsed.tv_nsec < stats->delay) {
		struct timespec remaining = {0, 0};

		/* remaining delay, _relative_ to time_end */
		remaining.tv_nsec = stats->delay - elapsed.tv_nsec;
		fps_limit_dbg("Sleeping for: ns: %ld", remaining.tv_nsec);

		clock_nanosleep(CLOCK_MONOTONIC, 0, &remaining, NULL);
	}

	/* update the stats for the next iteration */
	clock_gettime(CLOCK_MONOTONIC, &stats->time_start);
}
#endif /* FPS_LIMIT_H */
